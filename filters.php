<div class="form-inline">
    <div class="row row-up">
        <div class="col-xs-6 col-sm-3 desplegable">
            <select id="brand" name="brand" class="selectpicker" data-live-search="true" title="Marca..." multiple>
                <?php
                connect_mssql();
                $brands = get_brands();
                foreach($brands as $brand) {
                ?>
                    <option value="<?php echo $brand['Code']; ?>"><?php echo utf8_encode($brand['Name']); ?></option>
                <?php
                }
                ?>
            </select>
            <button name="brand" class="btn clean-select"></button>
        </div>
        <div class="col-xs-6 col-sm-3 desplegable">
            <select id="family" name="family" class="selectpicker" data-live-search="true" title="Familia..." multiple>
                <?php
                $families = get_families();
                foreach($families as $family) {
                ?>
                    <option value="<?php echo $family['Code']; ?>"><?php echo utf8_encode($family['Name']); ?></option>
                <?php
                }
                ?>
            </select>
            <button name="family" class="btn clean-select"></button>
        </div>

        <div class="col-xs-6 col-sm-3 desplegable">
            <select id="section" name="section" class="selectpicker" data-live-search="true" title="Secci&oacute;n..." multiple>
                <?php
                $sections = get_sections();
                foreach($sections as $section) {
                ?>
                    <option value="<?php echo $section['Code']; ?>"><?php echo utf8_encode($section['Name']); ?></option>
                <?php
                }
                ?>
            </select>
            <button name="section" class="btn clean-select"></button>
        </div>

        <div class="col-xs-6 col-sm-3 desplegable">
            <select id="season" name="season" class="selectpicker" data-live-search="true" title="Temporada..." multiple>
                <?php
                $seasons = get_seasons();
                foreach($seasons as $season) {
                ?>
                    <option value="<?php echo $season['Code']; ?>"><?php echo $season['Name']; ?></option>
                <?php
                }
                ?>
            </select>
            <button name="season" class="btn clean-select"></button>
        </div>
    </div>
    <br>
    <div class="row row-down">
        <div class="col-sm-3">
            Sellout %
            <?php
                if(isset($_GET['sellout'])) {
                    $sellout = $_GET['sellout'];
                } else {
                    $sellout = 20;
                }
            ?>
            <input id="sellout_p" class="form-control" type="number" name="sellout" min="0" max="100" step="1" value="<?php echo $sellout; ?>"/>
        </div>
        <div class="col-sm-3">
            <?php 
                if(isset($_GET['date_sellout'])) {
                    $current_date = $_GET['date_sellout'];
                } else {
                    $current_date = date("d/m/Y");
                }
            ?>
            Recibido antes de
            <input id="datepicker" class="form-control" name="date_sellout" value="<?php echo $current_date ?>" readonly="readonly" type="text">
        </div>
        <div class="col-sm-3">
        </div>
        <div class="col-sm-3">
            <button id="filtrar" class="btn btn-primary">Aplicar filtros</button>
        </div>
    </div>
</div>