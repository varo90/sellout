<?php
    include('header.php');
    include('functions.php');
?>

    <div class="container">
        <?php include('filters.php'); ?>
        
        <table id="data-info" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Foto</th>
                    <th>Referencia</th>
                    <th>ID Farfetch</th>
                    <th>Descuento %</th>
                    <th>Marca</th>
                    <th>Temporada</th>
                    <th style="width: 60px;">C&oacute;digos</th>
                    <th>Entradas</th>
                    <th style="width: 10px;">Vendidas</th>
                    <th>Sellout</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Foto</th>
                    <th>Referencia</th>
                    <th>ID Farfetch</th>
                    <th>Descuento %</th>
                    <th>Marca</th>
                    <th>Temporada</th>
                    <th style="width: 60px;">C&oacute;digos</th>
                    <th>Entradas</th>
                    <th style="width: 10px;">Vendidas</th>
                    <th>Sellout</th>
                </tr>
            </tfoot>
        </table>
        <div id="updated"></div>
        <a id='see_sent' href="info-sent-view.php" class="btn btn-info btn-lg" role="button" aria-pressed="true">Ver linkados</a>
        <button id='sendinfo_button' type='button' class='btn btn-primary btn-lg'>Linkar a Farfetch</button>
    </div>
</body>
</html>
