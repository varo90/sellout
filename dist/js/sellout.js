$(document).ready(function () {
    /*var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });*/
});

$(document).on('click', ".clean-select", limpiar_select);
$(document).on('click', ".unlink-btn", unlink_ref);
$(document).on('click', "#filtrar", filtrar);
$(document).on('click', "#sendinfo_button", send_id_farfetch);

$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '< Ant',
    nextText: 'Sig >',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    maxDate: 'Today'
};
$.datepicker.setDefaults($.datepicker.regional['es']);
$(function () {
    $("#datepicker").datepicker();
});

function filtrar() {
    var brands = [];
    $.each($("#brand option:selected"), function(){
        brands.push($(this).val());
    });
    var families = [];
    $.each($("#family option:selected"), function(){
        families.push($(this).val());
    });
    var sections = [];
    $.each($("#section option:selected"), function(){
        sections.push($(this).val());
    });
    var seasons = [];
    $.each($("#season option:selected"), function(){
        seasons.push($(this).val());
    });
    var sellout = $('#sellout_p').val();
    var date = $('#datepicker').val();

    datatable(brands,families,sections,seasons,sellout,date);
    //alert("You have selected the targets - " + targets.join(", "))
}

function limpiar_select() {
    var id = '#' + $(this).attr('name');
    $( id ).val('default');
    $( id ).selectpicker('refresh');
}

function unlink_ref() {
    var id = $(this).attr('name');
    $.ajax({
        url: 'unlink_ref.php',
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        data: {id:id},
        success: function( data ){
            $('#pndt__'+id).html( data );
            $('#dscn__'+id).html( data );
        }
    });
    
}

function datatable(brands,families,sections,seasons,sellout,date) {
    $('#data-info').dataTable().fnDestroy();

    $('#data-info').dataTable({
        "bProcessing": true,
        "sAjaxSource": "get-info.php",
        "aaSorting": [[9,'desc']],
        "lengthMenu": [[10, 25, 50, 100, 150, 200], [10, 25, 50, 100, 150, 200]],
        "oLanguage": {
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch": "Buscar:",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        "fnServerParams": function ( aoData ) {
            aoData.push({"name": "brands", "value": brands});
            aoData.push({"name": "families", "value": families});
            aoData.push({"name": "sections", "value": sections});
            aoData.push({"name": "seasons", "value": seasons});
            aoData.push({"name": "sellout", "value": sellout});
            aoData.push({"name": "date", "value": date});
        },
        "aoColumns": [
            { mData: 'picture' },
            { mData: 'reference' },
            { mData: 'input_frftch' },
            { mData: 'input_discount' },
            { mData: 'brand' },
            { mData: 'season' },
            { mData: 'codes' },
            { mData: 'entrance' },
            { mData: 'sold' },
            { mData: 'sellout' }
        ]
    });
}

function send_id_farfetch() {
    var ids = new Array();
    $('#data-info').find(':input.dsc').each(function(){
        //var input = $(this).val();
        var id_link = $(this).parent().parent().find(':nth-child(3)').find(':input.numinput').val();
        var id_sap = $(this).parent().parent().find(':nth-child(2)').text();
        if(id_link == null) {
            var id_link = $(this).parent().parent().parent().find(':nth-child(3)').text();
            var id_sap = $(this).parent().parent().parent().find(':nth-child(2)').text();
        }
        if(id_link !== '' && id_link != null) {
            var discount = $(this).val();
            ids.push({id_link:id_link, id_sap:id_sap, discount:discount});
        }
    });
    $.ajax({
        url: 'send_id_farfetch.php',
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        data: {ids:ids},
        success: function( data ){
            $('#updated').html( data );
            $('#data-info').DataTable().ajax.reload();
        }
    });
}