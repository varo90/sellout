<?php
    include('db_connections.php');
    include('session_init.php');

    $db = 'farfetch';
    $conn = mysql_connection($db);

    $ids = $_POST['ids'];
    //$ids[] = array('id_link' => '12095053','id_sap' => '805-000328-02','discount' => '0');
    
    $query_mysql = $conn->prepare("SELECT * FROM correspondencias WHERE ID_SAP=? LIMIT 1");
    
    // Prepare query and bind variables
    $query_insert = $conn->prepare("INSERT INTO correspondencias (ID_farfetch, ID_SAP, descuento, enviado, `insertado-por`, `fecha-insertado`) VALUES (:farfetch, :sap, :discount, :enviado, :insby, :dateins)");
    $query_insert->bindParam(':farfetch', $id_link);
    $query_insert->bindParam(':sap', $id_sap);
    $query_insert->bindParam(':discount', $discount);
    $query_insert->bindParam(':enviado', $sent);
    $query_insert->bindParam(':insby', $user);
    $query_insert->bindParam(':dateins', $date);
    
    $query_update = $conn->prepare("UPDATE correspondencias SET ID_farfetch = ?, descuento = ?, enviado = ?, `insertado-por` = ? WHERE ID_SAP = ?");
    
    $sent = 0;
    $user = $_SESSION['username_link'];
    $date = date('Y-m-d H:i:s');
    
    foreach($ids as $cont => $id) {
        $id_link = $id['id_link'];
        $id_sap = $id['id_sap'];
        $discount = $id['discount'];
        $query_mysql->execute(array($id_sap));
        $result = $query_mysql->fetch(PDO::FETCH_ASSOC);
        if($query_mysql->rowCount() == 0) {
            try {
                if($id_link != $result['ID_Farfetch'] && $id_sap != $result['ID_SAP']) {
                    $query_insert->execute();
                }
            }
            catch (PDOException $e) {
                echo 'No se pudo introducir uno de los registros: ' . $id_link . ' , ' . $id_sap . '. ' . $e->getMessage() . ' - ' . $date . ' - ' . $user . '<br>';
            }
        } else {
            try {
                if($id_link != $result['ID_Farfetch'] || $id_sap != $result['ID_SAP'] || $discount != $result['descuento']) {
                    //echo $id_link . '/' . $result['ID_Farfetch'] . ' // ' . $id_sap . '/' . $result['ID_SAP'] . ' // ' . $discount . '/' . $result['descuento'] . '<br>';
                    $state = $result['enviado'];
                    $who = explode('/',$result['insertado-por']);
                    if($state == '1') {
                        $state = '2';
                    }
                    if(!in_array($user, $who)) {
                        $user = $result['insertado-por'] . '/' . $user;
                    } else {
                        $user = $result['insertado-por'];
                    }
                    $query_update->execute(array($id_link,$discount,$state,$user,$id_sap));
                }
                $user = $_SESSION['username_link'];
            }
            catch (PDOException $e) {
                echo 'No se pudo introducir uno de los registros: ' . $id_link . ' , ' . $id_sap . '. ' . $e->getMessage() . ' - ' . $date . ' - ' . $user . '<br>';
            }
        }
    }
    echo '<font color="green"><b>Registros introducidos.</b></font>';
    
    mysql_disconnect($conn);
    
?>
