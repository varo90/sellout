<?php

    date_default_timezone_set('Europe/Madrid');
    include('db_connections.php');
    
    $db = 'farfetch';
    $conn = mysql_connection($db);
    
    $sql = "SELECT * FROM correspondencias ORDER BY `fecha-insertado`";
    
    $data = array();
    foreach ($conn->query($sql) as $row) {
        $id_link = $row['ID_Farfetch'];
        $id_sap = $row['ID_SAP'];
        $discount = $row['descuento'] . '%';
        $sent = $row['enviado'];
        $linked_by = $row['insertado-por'];
        $date_linked = $row['fecha-insertado'];
        if($sent == 0) {
            $sent = '<font color="red"><b>A&uacute;n no enviado</b></font>';
            $sent_by = '---';
            $date_sent = '---';
        } else {
            $sent = '<font color="green"><b>Enviado</b></font>';
            $sent_by = $row['enviado-por'];
            $date_sent = $row['fecha-enviado'];
        }
        $data[] = array('id_link'=>$id_link, 'id_sap'=>$id_sap, 'discount'=>$discount, 'sent'=>$sent, 'linked_by'=>$linked_by, 'date_linked'=>$date_linked, 'sent_by'=>$sent_by, 'date_sent'=>$date_sent);
    }
    
    mysql_disconnect($conn);

    $results = array(
        "sEcho" => 1,
        "iTotalRecords" => count($data),
        "iTotalDisplayRecords" => count($data),
        "aaData"=>$data
    );

    echo json_encode($results);