<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('session_init.php');
    include('db_connections.php');
    
    if (((empty($_SESSION['username_link']) || !isset($_SESSION['username_link']))
            && basename($_SERVER['SCRIPT_FILENAME']) != 'login.php')) {
        header("location:login.php");
    } 
    if((isset($_SESSION['username_link']) && $_SESSION['usergroup_link'] != 1)) {
        header("location:login.php");
    }

    date_default_timezone_set('Europe/Madrid');
    $server = 'srvsql';
    $link = mssql_connect($server, 'testdb', 'sesa6093');
    mssql_select_db('SBO_EULALIA', $link);
    
    $brands = @$_GET['brands'];
    $families = @$_GET['families'];
    $sections = @$_GET['sections'];
    $seasons = @$_GET['seasons'];
    $sellout = $_GET['sellout'];
    $date = $_GET['date'];

    if(!is_array($brands)) {
        $brands = array($brands);
    }
    if(!is_array($families)) {
        $families = array($families);
    }
    if(!is_array($sections)) {
        $sections = array($sections);
    }
    if(!is_array($seasons)) {
        $seasons = array($seasons);
    }
    if($sellout == '') {
        $sellout = 0;
    }

    $conditions = '';
    get_conditions($conditions,$brands,'oi.FirmCode') ;
    get_conditions($conditions,$families,'oi.U_GSP_COLLECTION') ;
    get_conditions($conditions,$sections,'oi.U_GSP_SECTION') ;
    get_conditions($conditions,$seasons,'oi.U_GSP_SEASON') ;
    
    $date_sellout = date_format(date_create_from_format('d/m/Y', $date), 'Y-m-d');
    
    $sql = "SELECT tb.reference,tb.color,tb.picture,tb.brand collate Modern_Spanish_CI_AS as brand,tb.season,tb.codes,coalesce(sum(tb.totalpdquantity),0)-coalesce(sum(tb.totaldevoquantity),0) AS bought, coalesce(sum(tb.totalenquantity),0)+coalesce(sum(tb.totalsaquantity),0) AS sold
            FROM
            (SELECT oi.CodeBars,tc.U_Gsp_Name as color,ipho.U_GSP_Photo1 AS picture,oi.U_GSP_REFERENCE as reference,omrc.FirmName as brand,oi.U_GSP_SEASON as season,en.totalenquantity,ve.totalsaquantity,pd.totalpdquantity,dv.totaldevoquantity,ig.orderquantity,
            CONCAT(tcmca.U_GSP_CATALOGNUM,CASE WHEN tcmca.U_GSP_MATERIALDESC IS NULL THEN '' ELSE CONCAT(' - ',tcmca.U_GSP_MATERIALDESC) END,
                    CASE WHEN tcmca.U_GSP_COLORDESC IS NULL THEN '' ELSE CONCAT('<br>',tcmca.U_GSP_COLORDESC) END) as codes
            FROM OITM oi with (NOLOCK)
            LEFT JOIN [dbo].[OMRC] omrc with (NOLOCK) ON oi.FirmCode = omrc.FirmCode 
            LEFT JOIN (select Codebars, sum(quantity) AS orderquantity FROM ign1 with (NOLOCK) WHERE (WhsCode='10' or WHsCode='01') group by codebars) as ig ON ig.Codebars=oi.Codebars
            LEFT JOIN (select Codebars, sum(quantity) AS totalpdquantity FROM pdn1 with (NOLOCK) WHERE ActDelDate<='$date_sellout' and Codebars<>'' GROUP BY codebars) AS pd on pd.Codebars=oi.Codebars
            LEFT JOIN (select Codebars, sum(quantity) AS totaldevoquantity FROM rpd1 with (NOLOCK) WHERE  Codebars<>'' GROUP BY codebars) as dv ON dv.Codebars=oi.Codebars 
            LEFT JOIN (select Codebars, sum(quantity) AS totalsaquantity FROM inv1 with (NOLOCK) GROUP BY codebars) AS ve ON oi.Codebars=ve.Codebars
            LEFT JOIN (select Codebars, sum(quantity) AS totalenquantity FROM dln1 with (NOLOCK) GROUP BY codebars) AS en ON oi.Codebars=en.Codebars
            LEFT JOIN dbo.[@GSP_TCMODELPHOTOS] ipho with (NOLOCK) ON ipho.U_GSP_ModelCode=oi.U_GSP_ModelCode AND oi.U_GSP_Color=ipho.U_GSP_Color
            LEFT JOIN dbo.[@GSP_TCCOLOR] tc with (NOLOCK) ON tc.Code=oi.U_GSP_Color
            LEFT JOIN [dbo].[@GSP_TCMODELCATALOG] tcmca with (NOLOCK) ON oi.U_GSP_MODELCODE = tcmca.U_GSP_MODELCODE and tcmca.U_GSP_MODELCOLOR = oi.U_GSP_Color
            WHERE $conditions)
            AS tb
            GROUP BY tb.reference, tb.color, tb.picture, tb.season, tb.brand, tb.codes";
    // oi.FirmCode = '' AND oi.U_GSP_COLLECTION = '' AND oi.U_GSP_SECTION = '' AND oi.U_GSP_SEASON = ''
    
    $query = mssql_query($sql);
    $items = array();
    while($row = mssql_fetch_array($query)) {
        $items[] = $row;
    }

    $db = 'farfetch';
    $conn = mysql_connection($db);
    $query_mysql = $conn->prepare("SELECT * FROM correspondencias WHERE ID_SAP=? LIMIT 1");
    
    $data = array();
    foreach($items as $row) {
        $bought = $row['bought'];
        $sold = $row['sold'];
        if($bought == 0) {
            $sellout_resp = '100';
        } else {
            $sellout_resp = ($sold*100)/$bought;
        }
        if($sellout_resp < $sellout && $bought > 0) {
            $sellout_str = number_format((float)($sold*100)/$bought, 2, ',', '');
            $id_sap = $row['reference'];
            $color = $row['color'];
            $reference = $id_sap . '-' . $color;
            $img_url = '..\Fotos_Sap\\' . $row['picture'];
            $imagen = "<a href=\"$img_url\" target=\"_blank\"><img src=\"$img_url\" alt=\"Sin imagen\" height=\"60\" width=\"60\"></a>";
            $query_mysql->execute(array($reference));
            if($query_mysql->rowCount() == 0) {
                $input_frftch = "<input class='numinput frf' type='number' name='$id_sap' value='' step='1' min='0' max='99999999'>";
                $input_discount = "<input class='numinput dsc' type='number' name='$id_sap-dis' value='0' step='1' min='0' max='100'>";
            } else {
                $ff_data =  $query_mysql->fetch(PDO::FETCH_ASSOC);
                $id_farfetch = $ff_data['ID_Farfetch'];
                $discount = $ff_data['descuento'];
                if($ff_data['enviado'] == '0') {
                    //$input_frftch = "<font color='orange'><b>Pendiente de linkar:</b><br>$id_farfetch</font>";
                    //$input_frftch = "<font color='orange'><b>Pendiente de linkar:</b></font><br><input class='numinput frf' type='text' name='$id_sap' value='$id_farfetch' step='1' min='0' max='99999999'>";
                    $input_frftch = "<div id='pndt__$reference'><font color='orange'><b>Pendiente de linkar:</b></font><br><div class='row'><div class='col-sm-8'><input class='numinput frf' type='text' name='$id_sap' value='$id_farfetch' step='1' min='0' max='99999999'></div><div class='col-sm-4'><button name='$reference' class='btn btn-danger unlink-btn'></button></div></div></div>";
                    $input_discount = "<div id='dscn__$reference'><input class='numinput dsc' type='number' name='$id_sap-dis' value='$discount' step='1' min='0' max='100'></div>";
                } else {
                    $input_frftch = '<div class="wrid">' . $id_farfetch . '</div>';
                    $input_discount = "<div id='dscn__$reference'><input class='numinput dsc' type='number' name='$id_sap-dis' value='$discount' step='1' min='0' max='100'></div>";
                    //$input_discount = $discount;
                }
            }
            $brand = utf8_encode($row['brand']);
            $season = $row['season'];
            $codes = utf8_decode($row['codes']);
            $data[] = array('picture'=>$imagen, 'reference'=>$reference, 'input_frftch'=>$input_frftch, 'input_discount'=>$input_discount, 'brand'=>$brand, 'season'=>$season, 'codes'=>$codes, 'entrance'=>$bought, 'sold'=>$sold, 'sellout'=>$sellout_str);
        }
    }
    
    mysql_disconnect($conn);

    $results = array(
        "sEcho" => 1,
        "iTotalRecords" => count($data),
        "iTotalDisplayRecords" => count($data),
        "aaData"=>$data
    );

    echo json_encode($results);
    
    
    function get_conditions(&$conditions,$filters,$sql_text) {
        if($filters[0] == '') {
            return false;
        }
        if($conditions !== '') {
            $conditions = $conditions . ' AND ';
        }
        $conditions = $conditions . '(';
        foreach($filters as $cont => $filter) {
            if($cont > 0) {
                $conditions = $conditions . ' OR ';
            }
            $conditions = $conditions . "$sql_text = '$filter'";
        }
        $conditions = $conditions . ')';
    }