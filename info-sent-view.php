<?php
    include('header.php');
?>

    <div class="contenedor">
        <table id="data-info" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>ID Farfetch</th>
                    <th>ID SAP</th>
                    <th>Descuento</th>
                    <th>Enviado</th>
                    <th>Linkado por</th>
                    <th>Fecha linkado</th>
                    <th>Enviado por</th>
                    <th>Fecha enviado</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>ID Farfetch</th>
                    <th>ID SAP</th>
                    <th>Descuento</th>
                    <th>Enviado</th>
                    <th>Linkado por</th>
                    <th>Fecha linkado</th>
                    <th>Enviado por</th>
                    <th>Fecha enviado</th>
                </tr>
            </tfoot>
        </table>
        <br>
        <a href="index.php" class="btn btn-info btn-lg" role="button" aria-pressed="true">Volver</a>
        <div id="updated"></div>
    </div>

    <script type="text/javascript" language="javascript" class="init">
        $( document ).ready(function() {
            $('#data-info').dataTable({
                "bProcessing": true,
                "sAjaxSource": "get-info-sent.php",
                "lengthMenu": [[10, 25, 50, 100, 150, 200, 500], [10, 25, 50, 100, 150, 200, 500]],
                "aaSorting": [[3,'asc'], [5,'desc']],
                "aoColumns": [
                    { mData: 'id_link' },
                    { mData: 'id_sap' },
                    { mData: 'discount' },
                    { mData: 'sent' },
                    { mData: 'linked_by' },
                    { mData: 'date_linked' },
                    { mData: 'sent_by' },
                    { mData: 'date_sent' }
                ]
            });   
        });
    </script>
</body>
</html>