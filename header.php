<?php
    include('session_init.php');
    if (((empty($_SESSION['username_link']) || !isset($_SESSION['username_link']))
            && basename($_SERVER['SCRIPT_FILENAME']) != 'login.php')) {
        header("location:login.php");
    } 
    if((isset($_SESSION['username_link']) && $_SESSION['usergroup_link'] != 1)) {
        header("location:login.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Sellout</title>

    <meta charset="utf-8">

    <link rel="stylesheet" href="dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/bootstrap-select.css">
    <link rel="stylesheet" href="dist/css/sellout.css">
    <link rel="stylesheet" href="dist/css/signin.css">
    <link rel="stylesheet" href="dist/css/jquery-ui.min.css">
    <link rel="stylesheet" href="dist/css/bootstrap-datepicker.min.css">
    <!-- Datatables -->
    <link href="dist/css/dataTables.responsive.css" rel="stylesheet">
    <link href="dist/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="dist/css/jquery.dataTables_themeroller.css" rel="stylesheet">

    <script src="dist/js/jquery.min.js"></script>
    <script src="dist/js/jquery-ui.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="dist/js/bootstrap-select.js"></script>
    <script src="dist/js/i18n/datepicker-es.js"></script>
    <script src="dist/js/sellout.js"></script>
    <!-- DataTables -->
    <script type="text/javascript" src="dist/js/dataTables.responsive.js"></script>
    <script type="text/javascript" src="dist/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="dist/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="dist/js/dataTables.buttons.min.js"></script>
</head>
<body>
<!-- Navigation -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                <a class="navbar-brand" href="../link/index.php"><img src='images/logo-se.png' alt='SE Logo'></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <?php
                    include('session_init.php');
                    if(empty($_SESSION['username_link']) || !isset($_SESSION['username_link'])) { 
                ?>
                        <li class="nav-item">
                          <a class="nav-link" href="login.php">Acceder</a>
                        </li>
                <?php
                }
                else { ?>
                        <li class="nav-item">
                          <a class="nav-link" href=""><?php echo $_SESSION['username_link']; ?></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="logout.php">Salir</a>
                        </li>
                <?php
                }
                ?>
              </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>