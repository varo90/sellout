<?php
    function connect_mssql() {
        date_default_timezone_set('Europe/Madrid');
        $server = 'srvsql';
        $link = mssql_connect($server, 'testdb', 'sesa6093');
        mssql_select_db('SBO_EULALIA', $link);
    }

    function get_brand_name($firmcode) {
        $sql = "SELECT FirmName FROM dbo.OMRC with (NOLOCK) WHERE FirmCode = $firmcode";
        return mssql_query($sql);
    }

    function get_brands() {
        //$sql = 'SELECT FirmCode as Code, FirmName COLLATE Modern_Spanish_CI_AS as Name FROM dbo.OMRC with (NOLOCK) WHERE FirmCode > -1';
        $sql = "SELECT om.FirmCode as Code,om.FirmName COLLATE Modern_Spanish_CI_AS as Name, SUM(ot.OnHand)
                FROM OITW ot
                LEFT JOIN OITM oi on oi.ItemCode = ot.ItemCode
                LEFT JOIN OMRC om on om.FirmCode = oi.FirmCode
                where ot.WhsCode='01' AND ot.OnHand>0
                group by om.FirmCode,om.FirmName
                order by om.FirmName";
        return make_query($sql);
    }

    function get_families() {
        $sql = 'SELECT Code, Code+" - "+Name COLLATE Modern_Spanish_CI_AS as Name FROM [dbo].[@GSP_TCCOLLECTION] with (NOLOCK) where Code>"299" order by Code';
        return make_query($sql);
    }

    function get_sections() {
        $sql = 'SELECT Code, Code+" - "+Name COLLATE Modern_Spanish_CI_AS as Name FROM [dbo].[@GSP_BSSECCION] with (NOLOCK) where Code>"2" order by Code';
        return make_query($sql);
    }

    function get_seasons() {
        $sql = 'SELECT Code, Name COLLATE Modern_Spanish_CI_AS as Name FROM [dbo].[@GSP_TCSEASON] with (NOLOCK) ORDER BY Name DESC';
        return make_query($sql);
    }
    
    function make_query($sql) {
        $query = mssql_query($sql);
        $results = array();
        while($row = mssql_fetch_array($query)) {
            $results[] = $row;
        }
        return $results;
    }